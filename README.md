# luajit-mail

[lua-resty-mail](https://github.com/GUI/lua-resty-mail)からOpenRestyに依存したコードを排除し

コアを[LuaSocket](https://luarocks.org/modules/luasocket/luasocket),[luajit-curl](https://luarocks.org/modules/sena-networks/luajit-curl)を選択可能にしました。

こちらのソースがオリジナルです。

https://luarocks.org/modules/gui/lua-resty-mail

# Install
`$ luarocks install luajit-mail`

# Dependency

[luajit-curl](https://luarocks.org/modules/sena-networks/luajit-curl) 1.1-0

[LuaSocket](https://luarocks.org/modules/luasocket/luasocket) 3.0rc1-2

[LuaSec](https://luarocks.org/modules/brunoos/luasec) 1.0.2-1

# Usage

## luasocket

```lua
local mail = require "luajit-mail"

local mailer = mail.new({
	host = "127.0.0.1",
	port = 1025,
})

local ok, err = mailer:send({
	from = "from@example.com",
	to = { "to2@example.com" },
	subject = "Subject",
	text = "Plain Text",
})

if not ok then
	print(err)
end
```

## luajit-curl

```lua
local mail = require "luajit-mail"

local mailer = mail.new({
	host = "127.0.0.1",
	port = 1025,
	module = "curl"
})

local ok, err = mailer:send({
	from = "from@example.com",
	to = { "to2@example.com" },
	subject = "Subject",
	text = "Plain Text",
})

if not ok then
	print(err)
end
```

## html mail,attachments,etc...

```lua
local mail = require "luajit-mail"

local mailer = mail.new({
	host = "127.0.0.1",
	port = 1025,
	module = "curl"
})

local ok, err = mailer:send({
	from = "From <from@example.com>",
	reply_to = "Reply <reply@example.com>",
	to = { "To <to@example.com>", "to2@example.com" },
	cc = { "Cc <cc@example.com>", "cc2@example.com" },
	bcc = { "Bcc <bcc@example.com>", "bcc2@example.com" },
	subject = "Subject",
	text = "Plain Text",
	html = "<p>HTML Text</p>",
	headers = {
		["X-Foo"] = "bar",
		["X-Bar"] = "baz",
	},
	attachments = {
		{
			filename = "foo.txt",
			content_type = "text/plain",
			content = "Hello, World.",
		},
		{
			filename = "foo.txt",
			content_type = "text/plain",
			content = "Hello, World.",
			disposition = "inline",
			content_id = "custom_content_id",
		},
	},
})

if not ok then
	print(err)
end
```

# Revesion

* 2021/10/11 1.0-4 osxでlibcryptoの読み込みが失敗する不具合を修正 バグ修正

* 2021/10/11 1.0-3 ミス修正

* 2021/10/07 1.0-2 libcryptを使用するよう修正

* 2021/10/07 1.0-1 libsslを使用するよう修正

* 2021/09/29 1.0-0 release
