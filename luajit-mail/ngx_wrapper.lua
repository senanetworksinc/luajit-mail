local socket = require "socket"
local ssl = require "ssl"
local ffi = require "ffi"
local ffi_new = ffi.new
local ffi_str = ffi.string
local ffi_null = ffi.null
local C = ffi.C

local b = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

ffi.cdef[[

enum { PCRE_CASELESS           = 0x00000001 };  /* C1       */
enum { PCRE_MULTILINE          = 0x00000002 };  /* C1       */
enum { PCRE_DOTALL             = 0x00000004 };  /* C1       */
enum { PCRE_EXTENDED           = 0x00000008 };  /* C1       */
enum { PCRE_ANCHORED           = 0x00000010 };  /* C4 E D   */
enum { PCRE_DOLLAR_ENDONLY     = 0x00000020 };  /* C2       */
enum { PCRE_EXTRA              = 0x00000040 };  /* C1       */
enum { PCRE_NOTBOL             = 0x00000080 };  /*    E D J */
enum { PCRE_NOTEOL             = 0x00000100 };  /*    E D J */
enum { PCRE_UNGREEDY           = 0x00000200 };  /* C1       */
enum { PCRE_NOTEMPTY           = 0x00000400 };  /*    E D J */
enum { PCRE_UTF8               = 0x00000800 };  /* C4        )          */
enum { PCRE_UTF16              = 0x00000800 };  /* C4        ) Synonyms */
enum { PCRE_UTF32              = 0x00000800 };  /* C4        )          */
enum { PCRE_NO_AUTO_CAPTURE    = 0x00001000 };  /* C1       */
enum { PCRE_NO_UTF8_CHECK      = 0x00002000 };  /* C1 E D J  )          */
enum { PCRE_NO_UTF16_CHECK     = 0x00002000 };  /* C1 E D J  ) Synonyms */
enum { PCRE_NO_UTF32_CHECK     = 0x00002000 };  /* C1 E D J  )          */
enum { PCRE_AUTO_CALLOUT       = 0x00004000 };  /* C1       */
enum { PCRE_PARTIAL_SOFT       = 0x00008000 };  /*    E D J  ) Synonyms */
enum { PCRE_PARTIAL            = 0x00008000 };  /*    E D J  )          */

/* This pair use the same bit. */
enum { PCRE_NEVER_UTF          = 0x00010000 };  /* C1        ) Overlaid */
enum { PCRE_DFA_SHORTEST       = 0x00010000 };  /*      D    ) Overlaid */

/* This pair use the same bit. */
enum { PCRE_NO_AUTO_POSSESS    = 0x00020000 };  /* C1        ) Overlaid */
enum { PCRE_DFA_RESTART        = 0x00020000 };  /*      D    ) Overlaid */

enum { PCRE_FIRSTLINE          = 0x00040000 };  /* C3       */
enum { PCRE_DUPNAMES           = 0x00080000 };  /* C1       */
enum { PCRE_NEWLINE_CR         = 0x00100000 };  /* C3 E D   */
enum { PCRE_NEWLINE_LF         = 0x00200000 };  /* C3 E D   */
enum { PCRE_NEWLINE_CRLF       = 0x00300000 };  /* C3 E D   */
enum { PCRE_NEWLINE_ANY        = 0x00400000 };  /* C3 E D   */
enum { PCRE_NEWLINE_ANYCRLF    = 0x00500000 };  /* C3 E D   */
enum { PCRE_BSR_ANYCRLF        = 0x00800000 };  /* C3 E D   */
enum { PCRE_BSR_UNICODE        = 0x01000000 };  /* C3 E D   */
enum { PCRE_JAVASCRIPT_COMPAT  = 0x02000000 };  /* C5       */
enum { PCRE_NO_START_OPTIMIZE  = 0x04000000 };  /* C2 E D    ) Synonyms */
enum { PCRE_NO_START_OPTIMISE  = 0x04000000 };  /* C2 E D    )          */
enum { PCRE_PARTIAL_HARD       = 0x08000000 };  /*    E D J */
enum { PCRE_NOTEMPTY_ATSTART   = 0x10000000 };  /*    E D J */
enum { PCRE_UCP                = 0x20000000 };  /* C3       */

/* Exec-time and get/set-time error codes */

enum { PCRE_ERROR_NOMATCH          = (-1) };
enum { PCRE_ERROR_NULL             = (-2) };
enum { PCRE_ERROR_BADOPTION        = (-3) };
enum { PCRE_ERROR_BADMAGIC         = (-4) };
enum { PCRE_ERROR_UNKNOWN_OPCODE   = (-5) };
enum { PCRE_ERROR_UNKNOWN_NODE     = (-5) };  /* For backward compatibility */
enum { PCRE_ERROR_NOMEMORY         = (-6) };
enum { PCRE_ERROR_NOSUBSTRING      = (-7) };
enum { PCRE_ERROR_MATCHLIMIT       = (-8) };
enum { PCRE_ERROR_CALLOUT          = (-9) };  /* Never used by PCRE itself */
enum { PCRE_ERROR_BADUTF8         = (-10) };  /* Same for 8/16/32 */
enum { PCRE_ERROR_BADUTF16        = (-10) };  /* Same for 8/16/32 */
enum { PCRE_ERROR_BADUTF32        = (-10) };  /* Same for 8/16/32 */
enum { PCRE_ERROR_BADUTF8_OFFSET  = (-11) };  /* Same for 8/16 */
enum { PCRE_ERROR_BADUTF16_OFFSET = (-11) };  /* Same for 8/16 */
enum { PCRE_ERROR_PARTIAL         = (-12) };
enum { PCRE_ERROR_BADPARTIAL      = (-13) };
enum { PCRE_ERROR_INTERNAL        = (-14) };
enum { PCRE_ERROR_BADCOUNT        = (-15) };
enum { PCRE_ERROR_DFA_UITEM       = (-16) };
enum { PCRE_ERROR_DFA_UCOND       = (-17) };
enum { PCRE_ERROR_DFA_UMLIMIT     = (-18) };
enum { PCRE_ERROR_DFA_WSSIZE      = (-19) };
enum { PCRE_ERROR_DFA_RECURSE     = (-20) };
enum { PCRE_ERROR_RECURSIONLIMIT  = (-21) };
enum { PCRE_ERROR_NULLWSLIMIT     = (-22) };  /* No longer actually used */
enum { PCRE_ERROR_BADNEWLINE      = (-23) };
enum { PCRE_ERROR_BADOFFSET       = (-24) };
enum { PCRE_ERROR_SHORTUTF8       = (-25) };
enum { PCRE_ERROR_SHORTUTF16      = (-25) };  /* Same for 8/16 */
enum { PCRE_ERROR_RECURSELOOP     = (-26) };
enum { PCRE_ERROR_JIT_STACKLIMIT  = (-27) };
enum { PCRE_ERROR_BADMODE         = (-28) };
enum { PCRE_ERROR_BADENDIANNESS   = (-29) };
enum { PCRE_ERROR_DFA_BADRESTART  = (-30) };
enum { PCRE_ERROR_JIT_BADOPTION   = (-31) };
enum { PCRE_ERROR_BADLENGTH       = (-32) };
enum { PCRE_ERROR_UNSET           = (-33) };

/* Specific error codes for UTF-8 validity checks */

enum { PCRE_UTF8_ERR0               = 0 };
enum { PCRE_UTF8_ERR1               = 1 };
enum { PCRE_UTF8_ERR2               = 2 };
enum { PCRE_UTF8_ERR3               = 3 };
enum { PCRE_UTF8_ERR4               = 4 };
enum { PCRE_UTF8_ERR5               = 5 };
enum { PCRE_UTF8_ERR6               = 6 };
enum { PCRE_UTF8_ERR7               = 7 };
enum { PCRE_UTF8_ERR8               = 8 };
enum { PCRE_UTF8_ERR9               = 9 };
enum { PCRE_UTF8_ERR10             = 10 };
enum { PCRE_UTF8_ERR11             = 11 };
enum { PCRE_UTF8_ERR12             = 12 };
enum { PCRE_UTF8_ERR13             = 13 };
enum { PCRE_UTF8_ERR14             = 14 };
enum { PCRE_UTF8_ERR15             = 15 };
enum { PCRE_UTF8_ERR16             = 16 };
enum { PCRE_UTF8_ERR17             = 17 };
enum { PCRE_UTF8_ERR18             = 18 };
enum { PCRE_UTF8_ERR19             = 19 };
enum { PCRE_UTF8_ERR20             = 20 };
enum { PCRE_UTF8_ERR21             = 21 };
enum { PCRE_UTF8_ERR22             = 22 };  /* Unused (was non-character) */

/* Specific error codes for UTF-16 validity checks */

enum { PCRE_UTF16_ERR0              = 0 };
enum { PCRE_UTF16_ERR1              = 1 };
enum { PCRE_UTF16_ERR2              = 2 };
enum { PCRE_UTF16_ERR3              = 3 };
enum { PCRE_UTF16_ERR4              = 4 };  /* Unused (was non-character) */

/* Specific error codes for UTF-32 validity checks */

enum { PCRE_UTF32_ERR0              = 0 };
enum { PCRE_UTF32_ERR1              = 1 };
enum { PCRE_UTF32_ERR2              = 2 };  /* Unused (was non-character) */
enum { PCRE_UTF32_ERR3              = 3 };

/* Request types for pcre_fullinfo() */

enum { PCRE_INFO_OPTIONS            = 0 };
enum { PCRE_INFO_SIZE               = 1 };
enum { PCRE_INFO_CAPTURECOUNT       = 2 };
enum { PCRE_INFO_BACKREFMAX         = 3 };
enum { PCRE_INFO_FIRSTBYTE          = 4 };
enum { PCRE_INFO_FIRSTCHAR          = 4 };  /* For backwards compatibility */
enum { PCRE_INFO_FIRSTTABLE         = 5 };
enum { PCRE_INFO_LASTLITERAL        = 6 };
enum { PCRE_INFO_NAMEENTRYSIZE      = 7 };
enum { PCRE_INFO_NAMECOUNT          = 8 };
enum { PCRE_INFO_NAMETABLE          = 9 };
enum { PCRE_INFO_STUDYSIZE         = 10 };
enum { PCRE_INFO_DEFAULT_TABLES    = 11 };
enum { PCRE_INFO_OKPARTIAL         = 12 };
enum { PCRE_INFO_JCHANGED          = 13 };
enum { PCRE_INFO_HASCRORLF         = 14 };
enum { PCRE_INFO_MINLENGTH         = 15 };
enum { PCRE_INFO_JIT               = 16 };
enum { PCRE_INFO_JITSIZE           = 17 };
enum { PCRE_INFO_MAXLOOKBEHIND     = 18 };
enum { PCRE_INFO_FIRSTCHARACTER    = 19 };
enum { PCRE_INFO_FIRSTCHARACTERFLAGS = 20 };
enum { PCRE_INFO_REQUIREDCHAR      = 21 };
enum { PCRE_INFO_REQUIREDCHARFLAGS = 22 };
enum { PCRE_INFO_MATCHLIMIT        = 23 };
enum { PCRE_INFO_RECURSIONLIMIT    = 24 };
enum { PCRE_INFO_MATCH_EMPTY       = 25 };

/* Request types for pcre_config(). Do not re-arrange, in order to remain
compatible. */

enum { PCRE_CONFIG_UTF8                    = 0 };
enum { PCRE_CONFIG_NEWLINE                 = 1 };
enum { PCRE_CONFIG_LINK_SIZE               = 2 };
enum { PCRE_CONFIG_POSIX_MALLOC_THRESHOLD  = 3 };
enum { PCRE_CONFIG_MATCH_LIMIT             = 4 };
enum { PCRE_CONFIG_STACKRECURSE            = 5 };
enum { PCRE_CONFIG_UNICODE_PROPERTIES      = 6 };
enum { PCRE_CONFIG_MATCH_LIMIT_RECURSION   = 7 };
enum { PCRE_CONFIG_BSR                     = 8 };
enum { PCRE_CONFIG_JIT                     = 9 };
enum { PCRE_CONFIG_UTF16                  = 10 };
enum { PCRE_CONFIG_JITTARGET              = 11 };
enum { PCRE_CONFIG_UTF32                  = 12 };
enum { PCRE_CONFIG_PARENS_LIMIT           = 13 };

/* Request types for pcre_study(). Do not re-arrange, in order to remain
compatible. */

enum { PCRE_STUDY_JIT_COMPILE                = 0x0001 };
enum { PCRE_STUDY_JIT_PARTIAL_SOFT_COMPILE   = 0x0002 };
enum { PCRE_STUDY_JIT_PARTIAL_HARD_COMPILE   = 0x0004 };
enum { PCRE_STUDY_EXTRA_NEEDED               = 0x0008 };

/* Bit flags for the pcre[16|32]_extra structure. Do not re-arrange or redefine
these bits, just add new ones on the end, in order to remain compatible. */

enum { PCRE_EXTRA_STUDY_DATA             = 0x0001 };
enum { PCRE_EXTRA_MATCH_LIMIT            = 0x0002 };
enum { PCRE_EXTRA_CALLOUT_DATA           = 0x0004 };
enum { PCRE_EXTRA_TABLES                 = 0x0008 };
enum { PCRE_EXTRA_MATCH_LIMIT_RECURSION  = 0x0010 };
enum { PCRE_EXTRA_MARK                   = 0x0020 };
enum { PCRE_EXTRA_EXECUTABLE_JIT         = 0x0040 };

struct real_pcre8_or_16;          /* declaration; the definition is private  */
typedef struct real_pcre8_or_16 pcre;

typedef struct pcre_extra {
   unsigned long int flags;        /* Bits for which fields are set */
   void *study_data;               /* Opaque data from pcre_study() */
   unsigned long int match_limit;  /* Maximum number of calls to match() */
   void *callout_data;             /* Data passed back in callouts */
   const unsigned char *tables;    /* Pointer to character tables */
   unsigned long int match_limit_recursion; /* Max recursive calls to match() */
   unsigned char **mark;           /* For passing back a mark pointer */
   void *executable_jit;           /* Contains a pointer to a compiled jit code */
 } pcre_extra;

extern pcre *pcre_compile(const char *, int, const char **, int *,
                  const unsigned char *);

extern int  pcre_fullinfo(const pcre *, const pcre_extra *, int,
                  void *);

extern int  pcre_exec(const pcre *, const pcre_extra *, const char *,
                   int, int, int, int *, int);

extern void  (*pcre_free)(void *);
]]

local libpcre = assert(ffi.load("libpcre"), "can't read libpcre!")


local _M = {}

_M.ERR = "error"

_M.re = {
	match = function(subject, regex, options)
		local err = ffi_new("const char *[1]")

		local error_offset = ffi_new("int[1]")

		local re_options = 0

		local re = libpcre.pcre_compile(regex, re_options, err, error_offset, nil)

		if re == ffi_null then
			if err ~= ffi_null then
				print(ffi_str(err))
			end

			return nil
		end

		local capture_count = ffi_new("int[1]")

		libpcre.pcre_fullinfo(re, ffi_null, libpcre.PCRE_INFO_CAPTURECOUNT, capture_count)

		local alloc_size = (capture_count[0] + 1) * 3

		local matches = ffi_new("int[" .. alloc_size .. "]")

		local n_matches = libpcre.pcre_exec(re, ffi_null, subject, #subject, 0, 0, matches, alloc_size)

		local ret = nil

		if n_matches > 0 then
			ret = {}

			do
				local s = matches[0 * 2] + 1
				local e = matches[0 * 2 + 1]

				ret[0] = subject:sub(s, e)
			end

			for i = 1, n_matches - 1 do
				local s = matches[i * 2] + 1
				local e = matches[i * 2 + 1]
				local tmp = subject:sub(s, e)

				table.insert(ret, tmp)
			end
		end

		libpcre.pcre_free(re)

		return ret
	end
}

_M.socket = {
	tcp = function()
		local _TCP = {}

		_TCP.__index = _TCP

		local instance = setmetatable({}, _TCP)

		instance._socket = socket.tcp()

		function _TCP:connect(address, port)
			return self._socket:connect(address, port)
		end

		function _TCP:send(data, ...)
			for k, v in ipairs(data) do
				self._socket:send(v, ...)
			end

			return true
		end

		function _TCP:receive()
			return self._socket:receive()
		end

		function _TCP:sslhandshake(reused_session, server_name, ssl_verify, send_status_req)
			self._socket = ssl.wrap(self._socket, { mode = "client", protocol = "tlsv1" })
			return self._socket:dohandshake()
		end

		function _TCP:settimeouts(value)
			self._socket:settimeout(value)
			return
		end

		function _TCP:close()
			return self._socket:close()
		end

		return instance
	end
}

function _M.encode_base64(str)
	return ((str:gsub('.', function(x)
		local r,b='',x:byte()
		for i=8,1,-1 do r=r..(b%2^i-b%2^(i-1)>0 and '1' or '0') end
		return r;
	end)..'0000'):gsub('%d%d%d?%d?%d?%d?', function(x)
		if (#x < 6) then return '' end
		local c=0
		for i=1,6 do c=c+(x:sub(i,i)=='1' and 2^(6-i) or 0) end
		return b:sub(c+1,c+1)
	end)..({ '', '==', '=' })[#str%3+1])
end

function _M.now()
	return os.time(os.date("!*t"))
end

function _M.log(log_level, ...)
	print(log_level, ...)
end

return _M
